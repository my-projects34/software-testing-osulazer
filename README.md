# Software testing = OSU!lazer

Project for software testing classes.

Repo contains presentation and testing checklist.

The Trello board (https://trello.com/b/ljrCzgjD/plan-test%C3%B3w) contains all details about the process of testing different OSU!lazer features:
- description,
- steps of testing,
- videos showing bugs and issues,
- links to github reported issues.

Email me if you want to look at the Trello board! (rafalbrozek98@gmail.com)

Project made by Rafał Brożek, Filip Sowa and Rafał Radwański.